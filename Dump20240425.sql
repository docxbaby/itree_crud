CREATE DATABASE  IF NOT EXISTS `itree` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `itree`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: localhost    Database: itree
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `musicalgenres`
--

DROP TABLE IF EXISTS `musicalgenres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musicalgenres` (
  `idmusicalGenres` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmusicalGenres`),
  UNIQUE KEY `idmusicalGenres_UNIQUE` (`idmusicalGenres`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musicalgenres`
--

LOCK TABLES `musicalgenres` WRITE;
/*!40000 ALTER TABLE `musicalgenres` DISABLE KEYS */;
INSERT INTO `musicalgenres` VALUES (1,'Pop'),(2,'Rock'),(3,'Hip Hop'),(4,'R&B'),(5,'Electrónica'),(6,'Reggae'),(7,'Jazz'),(8,'Country'),(9,'Clásica'),(10,'Folk');
/*!40000 ALTER TABLE `musicalgenres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restmessage`
--

DROP TABLE IF EXISTS `restmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restmessage` (
  `idrestmessage` int NOT NULL AUTO_INCREMENT,
  `message` varchar(45) NOT NULL,
  `active` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`idrestmessage`),
  UNIQUE KEY `idrestmessage_UNIQUE` (`idrestmessage`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restmessage`
--

LOCK TABLES `restmessage` WRITE;
/*!40000 ALTER TABLE `restmessage` DISABLE KEYS */;
INSERT INTO `restmessage` VALUES (1,'Buenas tardes ITREE',1);
/*!40000 ALTER TABLE `restmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `idroles` int NOT NULL AUTO_INCREMENT,
  `rolename` varchar(45) DEFAULT NULL,
  `roledesc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idroles`),
  UNIQUE KEY `idroles_UNIQUE` (`idroles`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'sysadmin','administrador del sistema'),(2,'appadmin','administrador de la app'),(3,'tester','usuario para pruebas'),(4,'user','usuario comun');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_musical_genre`
--

DROP TABLE IF EXISTS `user_musical_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_musical_genre` (
  `idusergenre` int NOT NULL AUTO_INCREMENT,
  `idgenre` int NOT NULL,
  `iduser` int NOT NULL,
  PRIMARY KEY (`idusergenre`),
  UNIQUE KEY `idusergenre_UNIQUE` (`idusergenre`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_musical_genre`
--

LOCK TABLES `user_musical_genre` WRITE;
/*!40000 ALTER TABLE `user_musical_genre` DISABLE KEYS */;
INSERT INTO `user_musical_genre` VALUES (12,1,1),(13,2,1),(14,3,1),(15,3,16),(16,5,16),(17,6,16),(18,6,18),(19,2,18);
/*!40000 ALTER TABLE `user_musical_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `iduser` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(70) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rolid` int DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `iduser_UNIQUE` (`iduser`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `userRol_idx` (`rolid`),
  CONSTRAINT `userRol` FOREIGN KEY (`rolid`) REFERENCES `roles` (`idroles`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'jpablo','f420bf2bbcc38d2a061790af9b6d3eb74d9d1960fd6f2a0fbf7548a730055b7e','Juan','Hernández','2024-04-24 12:41:48',3),(16,'wave','f420bf2bbcc38d2a061790af9b6d3eb74d9d1960fd6f2a0fbf7548a730055b7e','wave','noiser','2024-04-24 22:09:47',2),(17,'elon','f420bf2bbcc38d2a061790af9b6d3eb74d9d1960fd6f2a0fbf7548a730055b7e','Elon ','Musk','2024-04-24 22:17:12',3),(18,'mark','f420bf2bbcc38d2a061790af9b6d3eb74d9d1960fd6f2a0fbf7548a730055b7e','Mark','Zuckerberg','2024-04-24 22:19:31',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-25  8:40:20
