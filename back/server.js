const express = require('express');
const UsersController = require('./UsersController')
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const ms = require('ms')
const Mysql = require('./Connection')
const db = new Mysql();
const secretKey = 'itreeSecret';
const crypto = require('crypto');


const port = 4222;

app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({ origin: 'http://localhost:4200' }));


// Manejo de la ruta principal '/'
app.get('/', (req, res) => {
    res.send('Buenas tardes Itree');
});

app.post('/login', (req, res) => {
    const body = req.body;
    const user = body.user;
    const pass = body.password

    if (user != 'jpablo' || pass != 'Wavnoi2017') {
        res.send({ status: 'error', message: 'no se reconoce el usuario', cookies: req.cookies });
    } else {
        const token = jwt.sign(user, secretKey);
        res.cookie('token', token, { httpOnly: true, sameSite: 'None', secure: true });

        res.send({ status: 'success', message: 'usuario logeado', token: token })
    }

});

app.post('/login2', (req, res) => {
    const body = req.body;
    const user = body.user;
    const pass = body.password

    const query = 'select * from users where username = ?';

    db.preparedStmt(query, [user], (error, results) => {
        if (error) {
            res.send({ status: 'error', message: 'no se reconoce el usuario', cookies: req.cookies });
        } else {
            if (results.length > 0) {
                const dbpass = results[0].password;
                if (dbpass === encryptPassword(pass)) {
                    const token = jwt.sign(user, secretKey);
                    res.cookie('token', token, { httpOnly: true, sameSite: 'None', secure: true });
                    res.send({ status: 'success', message: 'usuario logeado', token: token })
                }
                else {
                    res.send({ status: 'error', message: 'no se reconoce el usuario', cookies: req.cookies });
                }
            } else {
                res.send({ status: 'error', message: 'no se reconoce el usuario', cookies: req.cookies });

            }
        }

    })

});

app.get('/message', (req, res) => {
    const sql = 'SELECT * FROM restmessage';
    db.connection.query(sql, (error, result) => {
        if (error) {
            // Error interno del servidor
            console.error('Error al obtener el mensaje:', error);
            res.status(500).json({ error: 'Error interno del servidor' });
            return;
        }

        if (result.length === 0) {
            // No se encontró el recurso
            res.status(404).json({ error: 'Mensaje no encontrado' });
            return;
        }

        // Se encontró el recurso
        const message = result[0].message;
        const htmlResponse = `
        <!DOCTYPE html>
        <html>
        <head>
            <title>Message</title>
        </head>
        <body>
            <h1>El mensaje es:</h1>
            <div style="display:grid; place-items:center; background-color:rgb(80,80,150); color:white; border-radius:.5rem">
            <h2 style="font-size:200px">${message}</h2>
            <div>
        </body>
        </html>
    `;
        res.status(200).send(htmlResponse);
    });
});

app.get('/usergenres', (req, res) => {
    const sql = 
    `select firstname, m.name 
    from user_musical_genre umg 
	    inner join musicalgenres m 
		    on umg.idgenre  = m.idmusicalgenres 
        inner join users u 
		    on umg.iduser = u.iduser`;

    db.connection.query(sql, (error, result) => {
        if (error) {
            // Error interno del servidor
            console.error('Error al obtener el mensaje:', error);
            res.status(500).json({ error: 'Error interno del servidor' });
            return;
        }

        if (result.length === 0) {
            // No se encontró el recurso
            res.status(404).json({ error: 'Mensaje no encontrado' });
            return;
        }

        let table = `<div style="display:grid; place-items:center;"><table style="border:1px solid black; padding:1rem;"><thead><tr><th>Usuario</th><th>Género</th></tr></thead><tbody>`;
        result.forEach(element => {
            table += `<tr><td>${element.firstname}</td><td>${element.name}</td></tr>`
        });
        table += `</tbody></table></div>`

        // Se encontró el recurso
        const htmlResponse = `
        <!DOCTYPE html>
        <html>
        <head>
            <title>Relacion de tablas de BD</title>
        </head>
        <body>` + table +
        
        `
        <p style="font-size:larger">
        Generado con la consulta siguiente 
        </p>
        <p style='font-family:monospace; font-size:larger; color:rgb(50,50,200)'>
        select 
            firstname, 
            m.name 
        from user_musical_genre umg 
	    inner join musicalgenres m 
		    on umg.idgenre  = m.idmusicalgenres 
        inner join users u 
		    on umg.iduser = u.iduser
        </p>
        </body>
        </html>
    `;
        res.status(200).send(htmlResponse);
    });
});

app.delete('/user', (req, res) => {
    console.log('delete')
    const strUserData = req.query.userData;
    const userData = JSON.parse(strUserData);
    userid = userData.userId;

    const query = 'delete from users where iduser = ?'
    db.preparedStmt(query, [userid], (error, results) => {
        if (error) {
            res.send({ status: 'error', message: 'No se pudo eliminar el registro' })
        } else {
            res.send({ status: 'success', message: 'se eliminó  correctamente' })
        }

    })
});


function checkToken(req, res, next) {
    const token = req.query.token || req.body.token;

    jwt.verify(token, secretKey, (error, usuario) => {
        if (error) {
            console.log("sucedio un error al validar token", token);
            console.log(error);
            return res.status(403).json({ mensaje: 'Token inválido', error: error, usuario: usuario });
        }

        req.usuario = usuario; // Guardar la información del usuario en el objeto de solicitud
        next();
    })
}


// Función para cifrar una contraseña
function encryptPassword(password) {
    const hash = crypto.createHash('sha256');
    hash.update(password);
    return hash.digest('hex');
}

app.get('/users', checkToken, (req, res) => {
    db.connection.query('select * from users, roles where users.rolid  = idroles ', (error, results) => {
        if (error) {
            res.send({ status: 'error', message: 'Hubo un error al obtener los usuarios' })
            return;
        }
        res.send({ status: 'success', users: results });
    })
})

app.post('/user/update', checkToken, (req, res) => {
    const userData = req.body.userData;
    const user = userData.user;
    const pass = userData.pass;
    const encrypted = encryptPassword(pass)
    const rolid = userData.rolSelected.rolid;
    const name = userData.name;
    const lname = userData.lname;
    const userId = userData.userId;

    const query = 'update users set password = ?, rolid = ?, firstname = ?, lastname = ?  where iduser = ? '
    db.preparedStmt(query, [encrypted, rolid, name, lname, userId], (error, result) => {
        if (error) {
            console.log(error)
            res.send({ status: 'error', message: 'No se pudo actualizar el usuario' });
            return;
        }
        res.send({ status: 'success', message: 'se actualizó el usuario' })

    })
})


app.post('/user/create', checkToken, (req, res) => {
    const query = 'insert into users ( username, password, firstname, lastname, rolid ) values ( ?, ?, ?, ?, ? )';
    const body = req.body;
    const user = body.user;
    const password = body.password;
    const encryptedPassword = encryptPassword(password);
    const firstname = body.name;
    const lastname = body.lastname;
    const rolid = body.rol;

    console.log('insertando')

    db.preparedStmt(query, [user, encryptedPassword, firstname, lastname, rolid], (error, results, fields) => {
        console.log('callback')

        if (error) {
            console.log(error)
            if (error.errno === 1062) {

                res.send({ status: 'error', message: 'Nombre de usuario no disponible' });
                return;
            }
            res.send({ status: 'error', message: 'Error executing query:' + error.sql });
            return;
        }

        res.send({ status: 'success', message: 'se agrego el nuevo usuario ' })
    });
})

app.get('/roles', checkToken, (req, res) => {
    db.connection.query('select * from roles', (err, rows) => {
        if (err)
            res.send({ status: 'error', message: err });
        if (rows)
            res.send({ status: 'success', message: rows })
    })

})



app.post('/validToken', (req, res) => {
    const token = req.body.token;
    jwt.verify(token, secretKey, (err, user) => {
        if (err) {
            res.send({ status: 'error' })
        }
        if (user) {
            res.send({ status: 'success', message: 'token es correcto' });
        }
    })
})

app.post('/users', UsersController.create);

// Manejo de errores 404
app.use((req, res, next) => {
    res.status(404).send("Lo siento, no se encontró la página.");
});

// Manejo de errores internos del servidor
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Algo salió mal en el servidor!');
});

// Iniciar el servidor
app.listen(port, '0.0.0.0', () => {
    const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    console.log(`\n\nServidor ejecutándose en el puerto: ${port}, Zona Horaria: ${timeZone}`);
});


