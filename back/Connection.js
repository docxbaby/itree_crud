const mysql = require('mysql');

class MySQLConnection {

    // Create a connection to the MySQL database
    connection = mysql.createConnection({
        host: 'localhost',
        port: 3307,
        user: 'app',
        password: 'Wavnoi2017',
        database: 'itree',
        authPlugins: {
            mysql_clear_password: () => () => Buffer.from('Wavnoi2017')
        }
    });

    constructor() {
        // Connect to the database
        this.connection.connect((err) => {
            if (err) {
                console.error('Error connecting to MySQL database:', err);
                return;
            }
            console.log('Connected to MySQL database');
        });
    }
    // Create a connection pool
    pool = mysql.createPool({
        host: 'localhost',
        port: 3307,
        user: 'app',
        password: 'Wavnoi2017',
        database: 'itree',
    });


    preparedStmt(query, params, callback) {
        this.pool.query(query, params, callback);
    }

}



module.exports = MySQLConnection; 
