import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import Swal from 'sweetalert2';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrl: './delete-user.component.sass'
})
export class DeleteUserComponent {
  userData: any;
  constructor(private http:HttpClient, private us:UsersService) {
    this.userData = {}
  }
  delete() {
    const token = localStorage.getItem('token');

    if (token){
      this.http.delete('http://localhost:4222/user',{ params:{ userData:JSON.stringify(this.userData), token:token }}).subscribe({next:(res:any)=>{
        if (res.status === 'success'){
          Swal.fire({title:'Eliminado',text:'Se eliminó el usuario', icon:'success'});
          this.us.emitter.emit(true);
        }
      }})
    }
  }

}
