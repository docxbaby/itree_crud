import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

export interface User {
  name: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.sass'
})
export class LoginComponent {
  

  constructor(private http:HttpClient){
    const token = localStorage.getItem('token');

    if (token){
      http.post('http://localhost:4222/validToken', { token: token }).subscribe((res:any)=>{
        if(res.status == 'success'){
          console.log('usuario logeado')
          this.logged = true; 
        }
      })
    }

  }

  

  logged = false;

  user = ''; 
  pass = '';

  display = true;

  sendLogin(){
    if (this.user.length  == 0)
      Swal.fire({title:'Error en el formulario', icon:'warning', text:'Debes colocar un nombre de usuario'});

    if (this.pass.length == 0)
      Swal.fire({title:'Error en el formulario', icon:'warning', text:'Debes colocar una contraseña'});

    // Dentro de tu componente
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  })};

    this.http.post('http://localhost:4222/login2',{ user:this.user, password:this.pass }, httpOptions ).subscribe((raw:any)=>{
      if(raw.status == 'error'){
        Swal.fire({title:'Usuario no encontrado', text:'Valida los datos introducidos', icon:'warning'})
      }
      if(raw.status === 'success'){
        localStorage.setItem('token', raw.token );
        this.logged = true;

      }
    })


  }


}
