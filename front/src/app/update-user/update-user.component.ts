import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import Swal from 'sweetalert2';

class User {
  userId: number = 0;
  user: string = '';
  pass: string = '';
  cpass: string = '';
  name: string = '';
  lname: string = '';
  rolSelected: { rolid: any; roledesc: any; } = { rolid: 0, roledesc: '' };
}

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrl: './update-user.component.sass'
})
export class UpdateUserComponent {

  userData: User = new User();
  roles: any[] = [];
  pass = '';
  cpass = '';

  constructor(private http: HttpClient) {
    this.getRoles();
  }

  getRoles() {
    const token = localStorage.getItem('token');

    if (token) {
      this.http.get('http://localhost:4222/roles', { params: { token: token } }).subscribe((res: any) => {
        if (res.status == 'success') {
          this.roles = res.message;
          console.log(this.roles)
        }
      })
    }

  }


  sendUpdateUser() {
    const token = localStorage.getItem('token');
    this.userData.pass = this.pass;
    this.userData.cpass = this.cpass;
    if (this.validator()) {

      this.http.post('http://localhost:4222/user/update', {
        userData: this.userData,
        token: token
      }).subscribe({
        next: (res: any) => {
          if (res.status === 'success') {
            Swal.fire({ title: 'Actualizado', text: 'se actualizó el usuario', icon: 'success' })
          } else {
            Swal.fire({ title: 'Error', text: 'No se pudo actualizar el usuario', icon: 'error' })
          }
        }
      })
    }
  }

  validator() {
    console.log('pass')
    console.log(this.userData.pass)
    console.log(this.userData.pass.length)

    if (this.userData.user.length < 4){
      Swal.fire({ title: 'Formato incorrecto', text: 'El nombre de usuario debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }
    
    if (this.userData.pass.length < 6){
      Swal.fire({ title: 'Formato incorrecto', text: 'la contraseña debe tener al menos 6 caracteres', icon: 'warning' });
      return false;
    }

    if (this.userData.name.length < 4){
      Swal.fire({ title: 'Formato incorrecto', text: 'el nombre debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }

    if (
      this.userData.lname.length < 4  ) {
      Swal.fire({ title: 'Formato incorrecto', text: 'El apellido debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }

    if (this.userData.cpass != this.userData.pass) {
      Swal.fire({ title: 'Formato incorrecto', text: 'Debes colocar la misma contraseña', icon: 'warning' });
      console.log('pass: ',  this.userData.pass, 'cpass: ', this.userData.cpass);
      return false;
    }

    if (this.userData.rolSelected.rolid === 0) {
      Swal.fire({ title: 'Formato incorrecto', text: 'Debes elegir un rol para el usuario', icon: 'warning' });
      return false;
    }
    return true;

  }




}
