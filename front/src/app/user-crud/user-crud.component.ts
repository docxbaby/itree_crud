import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import Swal from 'sweetalert2';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrl: './user-crud.component.sass'
})
export class UserCRUDComponent {
user = '';
pass = '';
cpass = '';
roles:any;
name = '';
lname = '';
rolSelected:number = 0;

constructor(private http: HttpClient, private us : UsersService){
  this.getRoles();
}

  getRoles(){
    const token = localStorage.getItem('token');

    if (token){
      this.http.get('http://localhost:4222/roles', {params:{token:token}}).subscribe((res:any)=>{
        if(res.status == 'success'){
          this.roles = res.message;
          console.log(this.roles)
        }      })
    }

  }

  validator() {

    if (this.user.length < 4){
      Swal.fire({ title: 'Formato incorrecto', text: 'El nombre de usuario debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }
    
    if (this.pass.length < 6){
      Swal.fire({ title: 'Formato incorrecto', text: 'la contraseña debe tener al menos 6 caracteres', icon: 'warning' });
      return false;
    }

    if (this.name.length < 4){
      Swal.fire({ title: 'Formato incorrecto', text: 'el nombre debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }

    if (
      this.lname.length < 4  ) {
      Swal.fire({ title: 'Formato incorrecto', text: 'El apellido debe tener al menos 4 caracteres', icon: 'warning' });
      return false;
    }

    if (this.cpass != this.pass) {
      Swal.fire({ title: 'Formato incorrecto', text: 'Debes colocar la misma contraseña', icon: 'warning' });
      return false;
    }

    if (this.rolSelected === 0) {
      Swal.fire({ title: 'Formato incorrecto', text: 'Debes elegir un rol para el usuario', icon: 'warning' });
      return false;
    }
    return true;

  }



  sendNewUser(){
    
    const token = localStorage.getItem('token');

    if (token && this.validator()){
      this.http.post('http://localhost:4222/user/create', { user:this.user, 
      password:this.pass, 
      name:this.name, 
      lastname:this.lname, 
      rol:this.rolSelected,
      token:token})

      .subscribe({next:(res:any)=>{
        if (res.status === 'success'){
          Swal.fire({title:'Exito', text:'Se agrego el nuevo usuario', icon:'success'});
          this.us.emitter.emit(true);
        }
        if(res.status == 'error'){
          
          Swal.fire({title:'Error', text:'No se pudo agregar el usuario: ' + res.message, icon:'error'});
        }
      }, 
    error:(error)=>{
      console.log(error)
    }})
      
    }
    else{
      console.log('no se obtuvo el token')
    }
    console.log({user:this.user, password:this.pass, name:this.name, lastname:this.lname, rol:this.rolSelected});


  }

}
