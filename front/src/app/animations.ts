import { trigger, state, style, transition, animate } from '@angular/animations';

const slideAnimation = trigger('slideAnimation', [
    state('initial', style({
      top: '-20%'
    })),
    state('final', style({
      top: '40%'
    })),
    transition('initial => final', animate('500ms ease-in')),
    transition('final => initial', animate('500ms ease-out'))
  ])

export default slideAnimation; 

export const contrast = trigger('contrast', [
  state('small', style({
    fontWeight: 'normal',
    fontSize: '16px'
  })),
  state('large', style({
    fontWeight:'bold',
    fontSize: '17px',
    color:"green",
    borderColor:"green"
  })),
  transition('small <=> large', animate('150ms ease-in-out')),
]);

export const fadeInOut = trigger('fadeInOut', [
  state('void', style({
    opacity: 0
  })),
  transition('void <=> *', animate(400)),
]);
