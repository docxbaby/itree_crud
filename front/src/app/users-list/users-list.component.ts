import { HttpClient } from '@angular/common/http';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UpdateUserComponent } from '../update-user/update-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UsersService } from '../users.service';
import { fadeInOut } from '../animations';


interface UserDataInterface {
}

class UserData extends MatTableDataSource<UserDataInterface> {
  constructor(initialData: UserDataInterface[]) {
    super(initialData);
  }
}


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrl: './users-list.component.sass',
  animations:[fadeInOut]
})
export class UsersListComponent implements AfterViewInit {
  userDataSource = new UserData([]);
  displayedColumns = ['id', 'userName', 'firstname', 'lastname', 'rol', 'created_at', 'actions'];
  
  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;
  @ViewChild(MatSort) sort: MatSort | null = null;
  
  constructor(private http: HttpClient, private _dialog: MatDialog, private us: UsersService) {
    this.getUsers()
    this.us.emitter.subscribe((event)=>{
      this.getUsers()
    })
  }

  openUpdate(userData:any) {
    const MatDialogRef:MatDialogRef<UpdateUserComponent> = this._dialog.open(UpdateUserComponent);
    console.log(userData)
    MatDialogRef.componentInstance.userData.name = userData.firstname;
    MatDialogRef.componentInstance.userData.lname = userData.lastname;
    MatDialogRef.componentInstance.userData.rolSelected = {rolid:userData.rolid, roledesc:userData.roledesc};
    MatDialogRef.componentInstance.userData.cpass = userData.password;
    MatDialogRef.componentInstance.userData.pass = userData.password;
    MatDialogRef.componentInstance.userData.user = userData.username;
    MatDialogRef.componentInstance.userData.userId = userData.iduser;

  }
  openDelete(userData:any) {
    const MatDialogRef:MatDialogRef<DeleteUserComponent> = this._dialog.open(DeleteUserComponent);
    MatDialogRef.componentInstance.userData.name = userData.firstname;
    MatDialogRef.componentInstance.userData.lname = userData.lastname;
    MatDialogRef.componentInstance.userData.rolSelected = { rolid: userData.rolid, rolename: userData.rolename, roledesc: userData.roledesc};
    MatDialogRef.componentInstance.userData.cpass = userData.password;
    MatDialogRef.componentInstance.userData.pass = userData.password;
    MatDialogRef.componentInstance.userData.user = userData.username;
    MatDialogRef.componentInstance.userData.userId = userData.iduser;
  }
  
  ngOnInit(): void {
    this.userDataSource.sort = this.sort;
  }


  applyFilter() {
    this.userDataSource.filter = this.searchText.trim().toLowerCase();
  }

  ngAfterViewInit(): void {
    // Establecer el paginador y el sorting después de la vista inicializada
    this.userDataSource.paginator = this.paginator;
  }



  users: any[] = []
  getUsers() {
    const token = localStorage.getItem('token');
    if (token) {
      this.http.get('http://localhost:4222/users', { params: { token: token } }).subscribe({
        next: (res: any) => {
          console.log('usuarios', res)
          if (res.status === 'success') {
            this.userDataSource.data = res.users;

          }
        }
      })

    }
  }
  searchText = '';

}
